SELECT s.["order_type"] as "Order Type",
    t.["month"],
    SUM(s.["profit"]) AS Profit,
    SUM(s.["quantity"]) AS Quantity
FROM [marsproject2].[dbo].[Sales] AS s
    JOIN [marsproject2].[dbo].[Product] prod
    ON s.["product_id"] = prod.["id"]
    JOIN [marsproject2].[dbo].[Time] AS t
    ON t.["id"] = s.["time_id"]
GROUP BY ROLLUP(s.["order_type"], t.["month"])
