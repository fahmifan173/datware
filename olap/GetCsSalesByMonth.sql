SELECT SUM(s.["profit"]) AS profit,
    SUM(s.["quantity"]) AS quantity,
    cs.["name"] AS Cs,
    t.["day"] AS 'Day'

FROM [marsproject2].[dbo].[Sales] AS s
    JOIN [marsproject2].[dbo].[Product] prod
    ON s.["product_id"] = prod.["id"]
    JOIN [marsproject2].[dbo].[Time] AS t
    ON t.["id"] = s.["time_id"]
    JOIN [marsproject2].[dbo].CustomerService AS cs
    ON (cs.["id"] = s.["cs_id"])
WHERE cs.["id"] = 1 AND t.["month"] = 4
GROUP BY cs.["name"], t.["month"], t.["day"]
ORDER BY t.["day"]