SELECT SUM(s.["profit"]) AS profit,
    SUM(s.["quantity"]) AS quantity,
    prod.["name"],
    t.["month"]
FROM [marsproject2].[dbo].[Sales] AS s
JOIN [marsproject2].[dbo].[Product] prod
    ON s.["product_id"] = prod.["id"]
JOIN [marsproject2].[dbo].[Time] AS t
    ON t.["id"] = s.["time_id"]
GROUP BY CUBE(prod.["name"], t.["month"])
