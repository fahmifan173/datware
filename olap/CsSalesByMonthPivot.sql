SELECT * FROM (
    SELECT cs.["name"] AS Cs, time.["month"] AS month, sales.["quantity"] AS qty
    FROM [marsproject2].[dbo].[Sales] AS sales
        JOIN [marsproject2].[dbo].[CustomerService] AS cs
        ON sales.["cs_id"] = cs.["id"]
        JOIN [marsproject2].[dbo].[Time] as time
        ON sales.["time_id"] = time.["id"]
) src
PIVOT (
    SUM(qty)
    FOR month in ([4], [5], [6])
) piv;
