./mockdata.js &&

echo ">>> creating csvs"

npm run json2csv -- -i ./mock/order.json -o ./mock/order.csv &&
npm run json2csv -- -i ./mock/vendor.json -o ./mock/vendor.csv &&
npm run json2csv -- -i ./mock/cs.json -o ./mock/cs.csv &&
npm run json2csv -- -i ./mock/products.json -o ./mock/products.csv &&
npm run json2csv -- -i ./mock/times.json -o ./mock/times.csv &&

echo ">>> success create csvs" && cd mock && ./gen-csv.sh;