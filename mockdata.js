#!/usr/bin/env node

const fs = require('fs');
const faker = require('faker');

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const csesName = ['Popy', 'Saras', 'Ira', 'Risa', 'Dwi', 'Dina', 'Mars'];
const cses = []
// seed cses
for (let i = 0; i < csesName.length; i++) {
  cses.push({
    id: i+1,
    name: csesName[i],
    age: Math.round(faker.random.number({min: 18, max: 27})),
    gender: 0,
  })
}

const vendors = [
  {
    id: 1,
    name: 'Dwita',
    district: 'Antapani, Bandung',
    village: 'Sari Asri'
  },
  {
    id: 2,
    name: 'Agus',
    district: 'Antapani, Bandung',
    village: 'Sari Asri'
  },
  {
    id: 3,
    name: 'Irv',
    district: 'Antapani, Bandung',
    village: 'Kintun'
  }
]

const prodModelsData = [
  'Swedish',
  'Candy',
  'Silver',
  'Elastico',
  'Snow',
  'Queenie',
  'Godiva',
  'Olivia',
  'Tim Tam Lacey',
  'Slipper',
  'Cassie',
  'Denim',
  'Tango',
  'Chit Chat',
  'Polka',
  'Aresa',
  'Lolipop'
];

const varian = [
  'White',
  'Black',
  'Sugar',
  'Almond',
  'Caramel',
  'Kuaci',
  'Marshmallow',
  'Tropical',
  'Tan',
  'Brown',
  'Red',
  'Pink',
  'Golden',
  'Dusty',
  'Maroon',
]

// seed prodModel
const prodModels = [];
for (let i = 0; i < prodModelsData.length; i++) {
  const cost  = parseFloat(faker.commerce.price(50000, 200000, 0))
  prodModels.push({
    id: i+1,
    name: prodModelsData[i],
    price: Math.round(cost + cost*0.8),
    vendor: vendors[getRandomInt(0, 2)]
  })
}

// seed products
const products = [];
let prodId = 0;
for (let i = 0; i < prodModels.length; i++) {
  for (let j = 0; j < 5; j++) {
    ++prodId;
    const prodPos = getRandomInt(0, prodModels.length - 1)
    products.push({
      id: prodId,
      prodModel: prodModels[prodPos].name,
      name: `${prodModels[prodPos].name} ${varian[getRandomInt(0, varian.length - 1)]}`,
      cost: Math.round(prodModels[prodPos].price - prodModels[prodPos].price*0.8),
      price: prodModels[prodPos].price,
    })
  }
}

// seed time
const times = [];
const month = [4, 5, 6];
const year = 2018;
let timeId = 0;
for (let j = 0; j < month.length; j++) {
  for (let i = 1; i <= 30; i++) {  
    ++timeId;
    times.push({
      id: timeId,
      day: i,
      month: month[j],
      year,
    })
  }
}

// seed order, j-month * k-date () * t-transcation
const orderStatus = ['readyStock', 'preOrder']
const orders = [];
// j-month
for (let j = 0; j < month.length; j++) {
  // k-date
  for (let k = 0; k < times.length; k++) {
    // t-transactions a day
    for (let t = 0; t < 20; t++) {
      const order_type = orderStatus[getRandomInt(0, 1)];
      const prod = products[getRandomInt(0, products.length - 1)];
      const cs = cses[getRandomInt(0, cses.length - 1)];
      const time = times[k]
      const quantity = Math.round(faker.random.number({min: 1, max: 7}));
      const vendor = vendors[getRandomInt(0, vendors.length - 1)];

      orders.push({
        time_id: time.id,
        cs_id: cs.id,
        vendor_id: vendor.id,
        product_id: prod.id,
        pay: prod.price + faker.random.number({ min: 50, max: 1000 }),
        quantity,
        profit: (quantity * prod.price) - prod.cost,
        order_type,
      });
    }
  }
}

const orderStr = JSON.stringify(orders, undefined, 2)
fs.writeFile('./mock/order.json', orderStr, err => {
  if (err) {
    console.log('Error writing file', err)
  } else {
    console.log('Successfully wrote file')
  }
})

// Writes to JSON file
const vendorsStr = JSON.stringify(vendors, undefined, 2)
fs.writeFile('./mock/vendor.json', vendorsStr, err => {
  if (err) {
    console.log('Error writing file', err)
  } else {
    console.log('Successfully wrote file')
  }
})

const csesStr = JSON.stringify(cses, undefined, 2)
fs.writeFile('./mock/cs.json', csesStr, err => {
  if (err) {
    console.log('Error writing file', err)
  } else {
    console.log('Successfully wrote file')
  }
})

const productsStr = JSON.stringify(products, undefined, 2)
fs.writeFile('./mock/products.json', productsStr, err => {
  if (err) {
    console.log('Error writing file', err)
  } else {
    console.log('Successfully wrote file')
  }
})

const timesStr = JSON.stringify(times, undefined, 2)
fs.writeFile('./mock/times.json', timesStr, err => {
  if (err) {
    console.log('Error writing file', err)
  } else {
    console.log('Successfully wrote file')
  }
});
