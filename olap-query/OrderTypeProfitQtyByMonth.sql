SELECT s.order_type as "Order Type",
   t.["month"],
   SUM(CAST(s.profit AS bigint)) AS Profit,
   SUM(s.quantity) AS Quantity
FROM [marsproject].[dbo].[Sales] AS s
   JOIN [marsproject].[dbo].[Product] prod
   ON s.product_id = prod.id
   JOIN [marsproject2].[dbo].[Time] AS t
   ON t.["id"] = s.time_id
GROUP BY ROLLUP(s.order_type, t.["month"])