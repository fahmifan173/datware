SELECT * 
FROM (
   SELECT cs.name AS Cs, time.month AS month, sales.quantity AS qty
   FROM [marsproject].[dbo].[Sales] AS sales
       JOIN [marsproject].[dbo].[CustomerService] AS cs
       ON sales.customer_service_id = cs.id
       JOIN [marsproject].[dbo].[Time] as time
       ON sales.time_id = time.id
) src
PIVOT (
   SUM(qty)
   FOR month in ([4], [5], [6])
) piv;
