SELECT prod.name AS "Product",
    AVG(s.quantity) AS "Avg Qty Sold",
    t.month
FROM [marsproject].[dbo].[Sales] AS s
    JOIN [marsproject].[dbo].[Product] prod
    ON s.product_id= prod.id
    JOIN [marsproject].[dbo].[Time] AS t
    ON t.id = s.time_id
GROUP BY prod.name, t.month
ORDER BY t.month