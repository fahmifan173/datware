1. Product Profit & Quantity by Month
SELECT SUM(CAST(s.profit as bigint)) AS profit,
   SUM(s.quantity) AS quantity,
   prod.name,
   t.month
FROM [marsproject].[dbo].[Sales] AS s
JOIN [marsproject].[dbo].[Product] prod
   ON s.product_id= prod.id
JOIN [marsproject].[dbo].[Time] AS t
   ON t.id = s.time_id
GROUP BY ROLLUP(prod.name, t.month)

2. Product Profit & Quantity of Cs in a Month 
SELECT SUM(s.profit) AS profit,
   SUM(s.quantity) AS quantity,
   cs.name AS Cs,
   t.day AS 'Day'

FROM [marsproject].[dbo].[Sales] AS s
   JOIN [marsproject].[dbo].[Product] prod
   ON s.product_id = prod.id
   JOIN [marsproject].[dbo].[Time] AS t
   ON t.id = s.time_id
   JOIN [marsproject].[dbo].CustomerService AS cs
   ON (cs.id = s.customer_service_id)
WHERE cs.id = 1 AND t.month = 4
GROUP BY cs.name, t.month, t.day
ORDER BY t.day

3.Cube Product Profit & Quantity by Month 
SELECT SUM(CAST(s.profit AS bigint)) AS profit,
   SUM(s.quantity) AS quantity,
   prod.name,
   t.month
FROM [marsproject].[dbo].[Sales] AS s
JOIN [marsproject].[dbo].[Product] prod
   ON s.product_id = prod.id
JOIN [marsproject].[dbo].[Time] AS t
   ON t.id = s.time_id
GROUP BY CUBE(prod.name, t.month)

4. Order Type Profit, Quantity by Month 
SELECT s.order_type as "Order Type",
   t.["month"],
   SUM(CAST(s.profit AS bigint)) AS Profit,
   SUM(s.quantity) AS Quantity
FROM [marsproject].[dbo].[Sales] AS s
   JOIN [marsproject].[dbo].[Product] prod
   ON s.product_id = prod.id
   JOIN [marsproject2].[dbo].[Time] AS t
   ON t.["id"] = s.time_id
GROUP BY ROLLUP(s.order_type, t.["month"])

5. Customer Service Sales by Month Pivoted 
SELECT * 
FROM (
   SELECT cs.name AS Cs, time.month AS month, sales.quantity AS qty
   FROM [marsproject].[dbo].[Sales] AS sales
       JOIN [marsproject].[dbo].[CustomerService] AS cs
       ON sales.customer_service_id = cs.id
       JOIN [marsproject].[dbo].[Time] as time
       ON sales.time_id = time.id
) src
PIVOT (
   SUM(qty)
   FOR month in ([4], [5], [6])
) piv;
