SELECT cs.name AS Cs,
    SUM(s.quantity) AS quantity,
    SUM(CAST(s.profit AS bigint)) AS profit,
    t.month AS 'Month'

FROM [marsproject].[dbo].[Sales] AS s
    JOIN [marsproject].[dbo].[Product] prod
    ON s.product_id = prod.id
    JOIN [marsproject].[dbo].[Time] AS t
    ON t.id = s.time_id
    JOIN [marsproject].[dbo].CustomerService AS cs
    ON cs.id = s.customer_service_id
GROUP BY ROLLUP (cs.name, t.month)
