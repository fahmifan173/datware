SELECT SUM(CAST(s.profit AS bigint)) AS profit,
   SUM(s.quantity) AS quantity,
   prod.name,
   t.month
FROM [marsproject].[dbo].[Sales] AS s
JOIN [marsproject].[dbo].[Product] prod
   ON s.product_id = prod.id
JOIN [marsproject].[dbo].[Time] AS t
   ON t.id = s.time_id
GROUP BY CUBE(prod.name, t.month)