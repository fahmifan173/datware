SELECT prod.name,
   SUM(s.quantity) AS quantity,
   SUM(CAST(s.profit as bigint)) AS profit,
   t.month
FROM [marsproject].[dbo].[Sales] AS s
JOIN [marsproject].[dbo].[Product] prod
   ON s.product_id= prod.id
JOIN [marsproject].[dbo].[Time] AS t
   ON t.id = s.time_id
GROUP BY ROLLUP(prod.name, t.month)
