echo ">>> zipping"
create_date=$(date +%Y-%m-%d);
create_time=$(date +"%T");
timestamp="${create_date}_${create_time}";

zip -r "mock-$timestamp.zip" cs.csv order.csv products.csv vendor.csv times.csv

echo ">>> success zipping"